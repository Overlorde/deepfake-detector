import logging
import os
import random
import cv2
import dlib
import numpy as np
import skimage
from skimage import feature


class Extract:
    """
        This class allows to extract the content used in Analyze such as video or images
    """

    def __init__(self, path_to_content=None, path_to_images=None, path_to_faces=None, path_to_align_faces=None,
                 shape_predictor=None):
        """
        Constructor of Extract
        :param path_to_content: the path to the user content
        :param path_to_images: the path to the folder of images
        :param path_to_faces: the path to the folder of faces
        :param path_to_align_faces: the path to the folder of aligned faces
        :param shape_predictor: the path to the shape predictor (used to crop the ROI on images)
        """
        self.content_input = None
        self.path_to_content = path_to_content
        self.path_to_images = path_to_images
        self.path_to_faces = path_to_faces
        self.path_to_align_faces = path_to_align_faces
        self.shape_predictor = shape_predictor

    def extract_content(self, time):
        """
        Extract the multiple frame if it is a video else init the content_input
        :param time: the time lapse between two frame (ms)
        :return:
        """
        try:
            if not os.path.exists(self.path_to_images):
                os.makedirs(self.path_to_images)
        except OSError:
            print('Error: Creating directory of images')
        if '\\' in self.path_to_content:
            separator = '\\'
        else:
            separator = '/'
        if ('.mp4' or '.avi' or '.gif') in self.path_to_content:
            self.content_input = cv2.VideoCapture(self.path_to_content)
            currentframe = 0
            while True:
                # reading from frame
                self.content_input.set(cv2.CAP_PROP_POS_MSEC, (currentframe * time))  # extract frame each x seconds
                ret, frame = self.content_input.read()
                if ret:
                    # if video is still left continue creating images
                    name = self.path_to_images + '/frame' + '_' + \
                           str(self.path_to_content).rsplit(separator, 1)[-1].split('.', 1)[0] + '_' \
                           + str(currentframe) + '.jpg'
                    logging.info('Creating...' + name)
                    # writing the extracted images
                    cv2.imwrite(name, frame)
                    # increasing counter so that it will
                    # show how many frames are created
                    currentframe += 1
                else:
                    break
            self.content_input.release()
            cv2.destroyAllWindows()
        else:
            name = self.path_to_images + separator + str(self.path_to_content).rsplit(separator, 1)[-1]
            logging.info('Creating... ' + name)
            self.content_input = cv2.imread(self.path_to_content, cv2.IMREAD_UNCHANGED)
            if self.content_input is None:
                print('Error : file not found.')
            else:
                try:
                    cv2.imwrite(name, self.content_input)
                except cv2.error as e:
                    print("An error occurred in Extract : {}".format(e))
                    exit(1)
                    raise

    def crop_face(self):
        """
        Crop the image with by keeping the face only
        :return: the bounding box corresponding to the ROI of the image
        """
        bounding_boxes = []
        if not os.path.exists(self.path_to_images):
            print("Error : Missing folder images in extract.")
        if not os.path.exists(self.path_to_faces):
            os.makedirs(self.path_to_faces)

        detector = dlib.get_frontal_face_detector()
        frames = [img for img in os.listdir(self.path_to_images)]
        for frame in frames:
            print("Processing file: {}".format(frame))
            img = cv2.imread(os.path.join(self.path_to_images, frame), cv2.IMREAD_UNCHANGED)
            # The 1 in the second argument indicates that we should upsample the image
            # 1 time.  This will make everything bigger and allow us to detect more
            # faces.
            detections = detector(img, 1)
            print("Number of faces detected: {}".format(len(detections)))
            for i, d in enumerate(detections):
                print("Detection {}: Left: {} Top: {} Right: {} Bottom: {}".format(
                    i, d.left(), d.top(), d.right(), d.bottom()))
                bounding_boxes.append([d.left(), d.top(), d.right(), d.bottom()])
                try:
                    if img is not None:
                        crop_image = img[d.top():d.bottom(), d.left():d.right()]
                        cv2.imwrite(os.path.join(self.path_to_faces, frame),
                                    crop_image)
                        print("File as been written in : {}".format(os.path.join(self.path_to_faces, frame)))
                except Exception as e:
                    print("An Error occurred during cropping in Extract : {}".format(e))
        return bounding_boxes

    def align_face(self):
        """
        Align the face of the image
        :return:
        """
        detector = dlib.get_frontal_face_detector()
        sp = None
        try:
            sp = dlib.shape_predictor(self.shape_predictor)
        except Exception as e:
            print("An error occurred in align_face : {}".format(e))
        # Load the image using Dlib
        if not os.path.exists(self.path_to_faces):
            print("Error : Folder faces is not found.")
            raise
        if not os.path.exists(self.path_to_align_faces):
            os.makedirs(self.path_to_align_faces)
        frames = [img for img in os.listdir(self.path_to_faces)]
        for frame in frames:
            img = cv2.imread(os.path.join(self.path_to_faces, frame), cv2.IMREAD_UNCHANGED)
            # possible to call change_scale to simulate deepfake
            # possible to call blur_face to simulate deepfake
            detections = detector(img, 1)
            num_faces = len(detections)
            if num_faces == 0:
                print("Sorry, there were no faces found in '{}'".format(self.path_to_faces))
            else:
                # Find the 68 face landmarks we need to do the alignment.
                faces = dlib.full_object_detections()
                for detection in detections:
                    faces.append(sp(img, detection))
                images = dlib.get_face_chips(img, faces, size=320)
                for i, image in enumerate(images):
                    cv2.imwrite(os.path.join(self.path_to_align_faces, frame),
                                image)

    def extract_co_occurrence_matrix(self):
        """
        Extract the color co-occurrence matrix from the image with 256*256 format
        :return: the co-occurrence matrices of the input image
        """
        image = cv2.imread(self.path_to_content, cv2.IMREAD_UNCHANGED)
        red_channel = np.array(image[:, :, 2], dtype=np.uint8)
        green_channel = np.array(image[:, :, 1], dtype=np.uint8)
        blue_channel = np.array(image[:, :, 0], dtype=np.uint8)
        # 0 -> 0° with 1 pixel offset to the right
        co_occurrence_matrix_0_red = skimage.feature.texture.greycomatrix(red_channel, [1], [0, 0],
                                                                          levels=256)[:, :, 0, 0]
        co_occurrence_matrix_45_red = skimage.feature.texture.greycomatrix(red_channel, [1], [0, np.pi / 4],
                                                                           levels=256)[:, :, 0, 0]  # pi/2 -> 45°
        co_occurrence_matrix_90_red = skimage.feature.texture.greycomatrix(red_channel, [1], [0, np.pi / 2],
                                                                           levels=256)[:, :, 0, 0]  # pi/2 -> 90°
        co_occurrence_matrix_135_red = skimage.feature.texture.greycomatrix(red_channel, [1], [0, (3 * np.pi) / 4],
                                                                            levels=256)[:, :, 0, 0]  # 3pi/4

        co_occurrence_matrix_0_green = skimage.feature.texture.greycomatrix(green_channel, [1], [0, 0],
                                                                            levels=256)[:, :, 0, 0]  # pi/2 -> 0°
        co_occurrence_matrix_45_green = skimage.feature.texture.greycomatrix(green_channel, [1], [0, np.pi / 4],
                                                                             levels=256)[:, :, 0, 0]  # pi/2 -> 45°
        co_occurrence_matrix_90_green = skimage.feature.texture.greycomatrix(green_channel, [1], [0, np.pi / 2],
                                                                             levels=256)[:, :, 0, 0]  # pi/2 -> 90°
        co_occurrence_matrix_135_green = skimage.feature.texture.greycomatrix(green_channel, [1], [0, (3 * np.pi) / 4],
                                                                              levels=256)[:, :, 0, 0]  # 3pi/4 -> 135°

        co_occurrence_matrix_0_blue = skimage.feature.texture.greycomatrix(blue_channel, [1], [0, 0],
                                                                           levels=256)[:, :, 0, 0]  # pi/2 -> 0°
        co_occurrence_matrix_45_blue = skimage.feature.texture.greycomatrix(blue_channel, [1], [0, np.pi / 4],
                                                                            levels=256)[:, :, 0, 0]  # pi/2 -> 45°
        co_occurrence_matrix_90_blue = skimage.feature.texture.greycomatrix(blue_channel, [1], [0, np.pi / 2],
                                                                            levels=256)[:, :, 0, 0]  # pi/2 -> 90°
        co_occurrence_matrix_135_blue = skimage.feature.texture.greycomatrix(blue_channel, [1], [0, (3 * np.pi) / 4],
                                                                             levels=256)[:, :, 0, 0]  # 3pi/4 -> 135°

        # return 4*256*256*3 vector
        return np.array([[co_occurrence_matrix_0_red, co_occurrence_matrix_0_green, co_occurrence_matrix_0_blue],
                         [co_occurrence_matrix_45_red, co_occurrence_matrix_45_green, co_occurrence_matrix_45_blue],
                         [co_occurrence_matrix_90_red, co_occurrence_matrix_90_green, co_occurrence_matrix_90_blue],
                         [co_occurrence_matrix_135_red, co_occurrence_matrix_135_green, co_occurrence_matrix_135_blue]])

    def random_scale(self):
        """
        Apply a random scale to the image
        :return: the image with a random scale
        """
        scale_percent = random.randint(1, 100)
        width = int(cv2.imread(self.path_to_content, cv2.IMREAD_UNCHANGED).shape[1] * scale_percent / 100)
        height = int(cv2.imread(self.path_to_content, cv2.IMREAD_UNCHANGED).shape[0] * scale_percent / 100)
        return cv2.resize(cv2.imread(self.path_to_content, cv2.IMREAD_UNCHANGED), (width, height),
                          interpolation=cv2.INTER_AREA)

    def blur_face(self):
        """
        Apply a gaussian filter to the image with 5 * 5 patch
        :return: the blurred image
        """
        return cv2.GaussianBlur(cv2.imread(self.path_to_content, cv2.IMREAD_UNCHANGED), (5, 5), 0)

    def affine_face(self, image):
        """
        Warp back the face on the original image
        :param image: the new image to warp back on the original
        :return: the original image with the new image placed on the original ROI
        """
        return cv2.warpAffine(src=image, flags=cv2.WARP_INVERSE_MAP, dsize=np.shape(image), dst=self.path_to_content,
                              M=cv2.invertAffineTransform(image))

    def random_alteration_face(self):
        """
        Apply a random alteration on the face of the image
        :return: the image with a random alteration transformation such as contrast or brightness
        """
        kernel_sharpening = np.array([[-1, -1, -1],
                                      [-1, 9, -1],
                                      [-1, -1, -1]])
        alpha = 1.0  # Simple contrast control
        beta = 0  # Simple brightness control
        # Initialize values
        print(' Basic Linear Transforms ')
        print('-------------------------')
        try:
            alpha = float(input(random.uniform(1.0, 3.0)))
            beta = int(input(random.uniform(0, 100)))
        except ValueError:
            print('Error, not a number')
        new_image = cv2.convertScaleAbs(cv2.imread(self.path_to_content, cv2.IMREAD_UNCHANGED), alpha=alpha, beta=beta)
        sharpen_image = cv2.filter2D(new_image, -1, kernel_sharpening)
        return sharpen_image
