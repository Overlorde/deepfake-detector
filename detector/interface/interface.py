# -*- coding: utf-8 -*-
import io
import math
from threading import Thread
import cv2
import numpy
import wx
import os
import shutil
from wx.lib.filebrowsebutton import FileBrowseButton
from definitions import ROOT_DIR
from detector.analyze.analyze import Analyze
from detector.extract.extract import Extract

HEIGHT_IMAGE = int(224 * 1.4)
WIDTH_IMAGE = int(224 * 1.4)

APP_EXIT = 1
WIDTH = 620  # width of the application
HEIGHT = 700  # height of the application


class View(wx.Frame):
    """
        This class handle the view part of the GUI
    """

    def __init__(self, *args, **kwargs):
        """
        Constructor of View
        :param args: the args
        :param kwargs: the kwargs
        """
        super(View, self).__init__(*args, **kwargs)
        application_path = application_path = os.path.join(ROOT_DIR, 'detector/extract')
        self.controller = Controller(path=None, path_to_images=os.path.join(application_path, 'images'),
                                     path_to_faces=os.path.join(application_path, 'faces'),
                                     path_to_align_faces=os.path.join(application_path, 'align-faces'),
                                     path_to_shape_predictor=os.path.join(application_path,
                                                                          'shape_predictor_68_face_landmarks.dat'))
        self.menu_bar = wx.MenuBar()
        self.panel = wx.Panel(self, wx.ID_ANY)
        self.button_browse = None
        self.image_artifact = numpy.zeros((WIDTH_IMAGE, HEIGHT_IMAGE))
        self.confidence_text = wx.StaticText(self.panel, label="")
        self.result_text = wx.StaticText(self.panel, label="")
        self.image_container = wx.StaticBitmap(self.panel, wx.ID_ANY, wx.Bitmap(
            wx.Image(io.BytesIO(cv2.imencode('.jpg', self.image_artifact)[1].tostring()))),
                                               wx.DefaultPosition, wx.DefaultSize, 0)
        # LAUNCH BUTTON
        self.button_launch = wx.Button(self.panel, -1, 'Lancer la détection')
        self.button_launch.Disable()
        self.init_ui()
        self.dialog = None

    def init_ui(self):
        """
        Function initializing all the elements inside the GUI panel
        :return:
        """
        # ICON
        icon = wx.Icon()
        if wx.Bitmap('icon.ico').IsOk():
            icon.CopyFromBitmap(wx.Bitmap("icon.ico", wx.BITMAP_TYPE_ANY))
        self.SetIcon(icon)
        # HELP MENU
        help_menu = wx.Menu()
        help_item1 = wx.MenuItem(help_menu, wx.ID_ABOUT, '&A Propos')
        help_item2 = wx.MenuItem(help_menu, wx.ID_HELP, '&Consulter l\'aide')
        help_menu.Append(help_item1)
        help_menu.Append(help_item2)
        # FILE MENU
        file_menu = wx.Menu()
        file_item = wx.MenuItem(file_menu, APP_EXIT, '&Quitter\tCtrl+Q')
        if wx.Bitmap('exit.png').IsOk():
            file_item.SetBitmap(wx.Bitmap('exit.png'))
        file_menu.Append(file_item)
        self.SetMenuBar(self.menu_bar)
        self.menu_bar.Append(file_menu, '&Fichier')
        self.menu_bar.Append(help_menu, '&Aide')
        # INIT SIZER
        sizer = wx.BoxSizer(wx.VERTICAL)
        # INIT STATIC BOX
        frame_top = wx.StaticBox(self.panel, -1, '')
        entry_box = wx.StaticBoxSizer(frame_top, wx.HORIZONTAL)
        frame_bottom = wx.StaticBox(self.panel, -1, '')
        result_box = wx.StaticBoxSizer(frame_bottom, wx.VERTICAL)
        # INIT BOX
        box_top = wx.BoxSizer(wx.VERTICAL)
        # SEARCH BUTTON
        self.button_browse = FileBrowseButton(self.panel, -1, buttonText='Rechercher', labelText='Emplacement :',
                                              toolTip='emplacement du contenu à identifier',
                                              fileMode=wx.FD_OPEN,
                                              dialogTitle='Sélectionner votre fichier',
                                              changeCallback=self.on_filename_changed,
                                              fileMask="*.png;*.jpg;*.mp4;*.avi;*.gif;*.jpeg;*.bmp",
                                              size=(500, 50))
        box_top.Add(self.button_browse, 0, wx.ALL | wx.UP | wx.EXPAND, 15)
        box_top.Add(self.button_launch, 0, wx.LEFT, 391)
        entry_box.Add(box_top, 0, wx.DOWN, 10)
        sizer.Add(entry_box, 0, wx.ALL | wx.CENTER, 5)  # | wx.EXPAND
        # TEXT 1
        static_text_1 = wx.StaticText(self.panel, label="Artefacts identifiés dans le contenu")
        font = wx.Font(10, int(wx.FONTFAMILY_DEFAULT), int(wx.FONTWEIGHT_NORMAL), int(wx.FONTWEIGHT_BOLD))
        static_text_1.SetFont(font)

        sizer.Add(static_text_1, 0, wx.ALL | wx.CENTER, 5)
        # IMAGE
        sizer.Add(self.image_container, 0, wx.ALL | wx.CENTER | wx.EXPAND, 5)
        # adding stretchable space before and after centers the image.
        # TEXT 2
        static_text_4 = wx.StaticText(self.panel, label="Résultat de la détection")
        font = wx.Font(10, int(wx.FONTFAMILY_DEFAULT), int(wx.FONTWEIGHT_NORMAL), int(wx.FONTWEIGHT_BOLD))
        static_text_4.SetFont(font)
        sizer.Add(static_text_4, 0, wx.ALL | wx.CENTER, 5)
        static_text_2 = wx.StaticText(self.panel, label="Status du contenu : ")
        static_text_3 = wx.StaticText(self.panel, label="Confiance dans la détection (en %) : ")
        font = wx.Font(10, int(wx.FONTFAMILY_DEFAULT), int(wx.FONTWEIGHT_NORMAL), int(wx.FONTWEIGHT_BOLD))
        self.result_text.SetFont(font)
        grid_result_box = wx.FlexGridSizer(2, 2, 0, 5)
        grid_result_box.Add(static_text_2, 0, wx.ALL, 15)
        grid_result_box.Add(self.result_text, 0, wx.ALL, 15)
        grid_result_box.Add(static_text_3, 0, wx.ALL, 15)
        grid_result_box.Add(self.confidence_text, 0, wx.ALL, 15)
        result_box.Add(grid_result_box, 0, wx.LEFT | wx.RIGHT, 150)
        sizer.Add(result_box, 0, wx.LEFT | wx.RIGHT | wx.CENTER, 30)
        # EVENT
        self.Bind(wx.EVT_BUTTON, self.on_button)
        self.button_browse.Bind(wx.EVT_CHECKBOX,
                                self.on_browse)
        self.Bind(wx.EVT_MENU, self.open_modal, help_item1)
        self.Bind(wx.EVT_MENU, self.open_modal, help_item2)
        self.Bind(wx.EVT_MENU, self.quit, id=APP_EXIT)
        self.Bind(wx.EVT_SIZING, self.on_resize)
        self.Bind(wx.EVT_MAXIMIZE, self.on_resize)
        self.Bind(wx.EVT_SIZE, self.on_resize)
        # END INIT PANEL ITEM
        self.panel.SetSizer(sizer)
        self.SetSize((WIDTH, HEIGHT))
        self.SetMinSize((WIDTH, HEIGHT))
        self.SetTitle('Détecteur de DeepFake')
        self.Centre()

    def on_resize(self, event):
        """
        Resize the image when the window is resized
        :param event: the resize event
        :return:
        """
        frame_h, frame_w = None, None
        frame_size = self.GetSize()
        is_resized = False
        if event.EventType == wx.EVT_SIZING.typeId:
            if (frame_size[0], frame_size[1]) > (WIDTH, HEIGHT):
                frame_w = (WIDTH_IMAGE + frame_size[0] - WIDTH)
                frame_h = (HEIGHT_IMAGE + frame_size[1] - HEIGHT)
        elif event.EventType == wx.EVT_MAXIMIZE.typeId:
            frame_w = WIDTH_IMAGE * 2.1
            frame_h = HEIGHT_IMAGE * 2.1
        elif event.EventType == wx.EVT_SIZE.typeId and frame_size[0] == WIDTH and frame_size[1] == HEIGHT:
            frame_w = WIDTH_IMAGE
            frame_h = HEIGHT_IMAGE
        if (frame_h and frame_w) is not None:
            image = cv2.resize(src=self.image_artifact, dsize=(int(frame_w), int(frame_h)),
                               interpolation=cv2.INTER_AREA)
            data = cv2.imencode('.jpg', image)[1].tostring()
            stream = io.BytesIO(data)
            self.image_container.SetBitmap(wx.Bitmap(wx.Image(stream)))
            self.image_container.SetPosition((150, 173))
            self.image_container.Fit()
            self.Refresh()
        event.Skip(True)

    def quit(self, event):
        """
        Allow to exit the application
        :param event: the windows event
        :return: the closure event windows
        """
        return self.Close()

    def on_browse(self, event):
        """
        Open the modal File dialog to select the input
        :param event: the dialog event
        :return:
        """
        wx.FileDialog(
            self, message="Choisir un fichier",
            defaultDir=os.getcwd(),
            defaultFile="",
            style=wx.FD_OPEN | wx.FD_CHANGE_DIR)

    def on_button(self, event):
        """
        Verify the file and launch the procedure of extraction and analyze
        :param event: the button event
        :return:
        """
        if os.path.exists(self.button_browse.GetValue()) and ('.mp4' in self.button_browse.GetValue()
                                                              or '.jpg' in self.button_browse.GetValue()
                                                              or '.png' in self.button_browse.GetValue()
                                                              or '.avi' in self.button_browse.GetValue()
                                                              or '.jpeg' in self.button_browse.GetValue()
                                                              or '.bmp' in self.button_browse.GetValue()):
            self.controller.path = self.button_browse.GetValue()
            self.controller.extract.path_to_content = self.controller.path
            self.controller.thread = ProgressDialogThread()
            self.controller.analyse = Analyze()  # reset analyse
            self.button_browse.SetValue('')
            self.controller.delete_temporary_files()
            self.controller.launch_extract()
            is_fake, confidence, artifact = self.controller.launch_analyze()
            self.update_confidence(is_fake, confidence)
            if artifact is not None:
                self.update_image(artifact=artifact)
        else:
            self.dialog = Dialog(title="Fenêtre d'Erreur")
            self.dialog.show_dialog()

    def open_modal(self, event):
        """
        Launch an event dialog such as about or help
        :param event: the menubar event
        :return:
        """
        if event.GetId() == wx.ID_HELP:
            self.dialog = Dialog(title="Fenêtre d'Aide")
        elif event.GetId() == wx.ID_ABOUT:
            self.dialog = Dialog(title="A Propos")
        if self.dialog is not None:
            self.dialog.show_dialog()

    def on_filename_changed(self, event):
        """
        Verify if the path is correct
        else disable the button_launch
        which launch the extraction and analyse
        :param event: the input event
        :return:
        """
        if os.path.exists(event.String):
            self.button_launch.Enable()
        else:
            self.button_launch.Disable()

    def update_confidence(self, is_fake, confidence):
        """
        Update the confidence and result on the GUI
        :param is_fake: a boolean which indicate if the content is pristine
        :param confidence: the confidence in the detection
        :return:
        """
        if confidence is not None:
            if is_fake:
                label = 'Falsifié'
                self.result_text.SetLabel(label)
                self.result_text.SetForegroundColour('SALMON')
            else:
                label = 'Non Falsifié'
                self.result_text.SetLabel(label)
                self.result_text.SetForegroundColour('LIME GREEN')
            self.confidence_text.SetLabel(str(confidence * 100).strip('[').strip(']') + ' %')

    def update_image(self, artifact):
        """
        Update the image with the ROI from the artifacts found on content
        :param artifact: the bounding box area (face)
        :return: nothing
        """
        if artifact is not None:
            frame_size = self.GetSize()
            image = cv2.resize(src=artifact,
                               dsize=((WIDTH_IMAGE + frame_size[0] - WIDTH), (HEIGHT_IMAGE + frame_size[1] - HEIGHT)),
                               interpolation=cv2.INTER_AREA)
            data = cv2.imencode('.jpg', image)[1].tostring()
            stream = io.BytesIO(data)
            self.image_artifact = artifact
            self.image_container.SetBitmap(wx.Bitmap(wx.Image(stream)))
            self.image_container.SetPosition((150, 173))
            self.image_container.Fit()
            self.Refresh()


class Dialog:
    """This class allows to create an Information Dialog"""

    def __init__(self, title):
        """Constructor of the information modal dialog"""
        self.dialog = None
        if title == "Fenêtre d'Aide":
            self.dialog = wx.GenericMessageDialog(None,
                                                  "--------------------------------------- PROTOCOLE D'UTILISATION DU "
                                                  "LOGICIEL "
                                                  "------------------------------------\n"
                                                  "1. Cliquez sur le bouton Rechercher\n"
                                                  "2. Indiquez l'emplacement du fichier ou son chemin dans la barre "
                                                  "de recherche \n "
                                                  "3. Les formats pris en compte sont :\n"
                                                  "* JPEG, JPG\n"
                                                  "* PNG\n"
                                                  "* BITMAP\n"
                                                  "* GIF\n"
                                                  "* MP4\n"
                                                  "* AVI\n"
                                                  "4. Appuyez sur le bouton Lancer la détection pour analyser le "
                                                  "contenu\n"
                                                  "5. Après analyse du contenu, un indicateur permet de connaître le "
                                                  "résultat de la détection avec le % de confiance\n"
                                                  "6. Une image permet de vérifier les emplacements où "
                                                  "la détection s'est appuyée pour formuler son résultat\n"
                                                  "------------------------------------------ LES CAUSES D'ERREURS "
                                                  "DU LOGICIEL "
                                                  "-------------------------------------\n"
                                                  "En cas d'affichage de la fenêtre d'erreur, plusieurs cas sont "
                                                  "possibles :\n "
                                                  "1. Erreur dans l'extraction de l'image (visage impossible à "
                                                  "extraire)\n "
                                                  "2. Erreur dans l'analyse de l'image (modèle manquant ou prise de "
                                                  "décision impossible)",
                                                  title)
        elif title == "A Propos":
            self.dialog = wx.GenericMessageDialog(None,
                                                  "1. Version du logiciel : 1.0\n"
                                                  "2. Auteur du logiciel : Alexandre Burnier-Framboret", title)
        elif title == "Fenêtre d'Erreur":
            self.dialog = wx.GenericMessageDialog(None,
                                                  "Une erreur logiciel dans l'analyse ou l'extraction n'a pas permis "
                                                  "de continuer.",
                                                  title)

    def show_dialog(self):
        """
        Allow to show the modal dialog and destroy when closed
        :return:
        """
        if self.dialog is not None:
            self.dialog.ShowModal()
            self.dialog.Destroy()


class ProgressDialogThread(Thread):
    """Test Worker Thread Class."""

    def __init__(self):
        """Init Worker Thread Class."""
        Thread.__init__(self)
        self.nb_step = 5
        self.max = (self.nb_step * (1 / self.nb_step) * 100)
        self.dialog = wx.ProgressDialog("Fenêtre de processus", "Extraction du contenu en cours...",
                                        self.max,
                                        style=wx.PD_CAN_ABORT | wx.PD_AUTO_HIDE | wx.PD_APP_MODAL | wx.PD_ELAPSED_TIME)
        self.count = 0
        self.should_abort = False
        self.start()

    def run(self):
        """
        Run Worker Thread.
        :return:
        """
        keep_going = True
        while keep_going and not math.isclose(self.count, self.max, rel_tol=1e-5):
            wx.Sleep(1)
            if self.dialog.WasCancelled():
                keep_going = False
                self.should_abort = True
            elif self.count == ((1 / self.nb_step) * 100):
                self.dialog.Update(self.count, "Extraction terminée.\nTraitement du contenu en cours...")
            elif ((2 / self.nb_step) * 100) <= self.count <= self.max:
                self.dialog.Update(self.count,
                                   "Extraction terminée.\nTraitement du contenu terminé.\nAnalyse "
                                   "du "
                                   "contenu en cours...")
        if self.count != self.max:
            self.dialog.Update(self.max)

    def update_progressbar(self):
        """
        Update the progress bar
        :return:
        """
        self.count += ((1 / self.nb_step) * 100)
        self.dialog.Update(self.count)


class Controller:
    """The Controller class of the application which handles all the stuffs with the model part and view"""

    def __init__(self, path, path_to_images, path_to_faces, path_to_align_faces, path_to_shape_predictor):
        """
        Constructor of Controller
        :param path: the path of the user content
        :param path_to_images: the path to the folder of images
        :param path_to_faces: the path to the folder of faces
        :param path_to_align_faces: the path to the folder of aligned faces
        :param path_to_shape_predictor: the path to the shape predictor (used to crop the ROI on images)
        """
        self.path = path
        self.path_to_images = path_to_images
        self.path_to_faces = path_to_faces
        self.path_to_align_faces = path_to_align_faces
        self.path_to_shape_predictor = path_to_shape_predictor
        self.analyse = Analyze()
        self.extract = Extract(path_to_content=self.path, path_to_images=self.path_to_images,
                               path_to_faces=self.path_to_faces,
                               path_to_align_faces=self.path_to_align_faces,
                               shape_predictor=self.path_to_shape_predictor)
        self.thread = None
        self.dialog = None

    def delete_temporary_files(self):
        """
        Delete the existing temporaries files
        :return:
        """
        if os.path.exists(self.path_to_faces):
            shutil.rmtree(self.path_to_faces)
        if os.path.exists(self.path_to_images):
            shutil.rmtree(self.path_to_images)
        if os.path.exists(self.path_to_align_faces):
            shutil.rmtree(self.path_to_align_faces)

    def launch_extract(self):
        """
        Launch the extraction of the input content
        :return: the bounding box found when cropping the face
        """
        try:
            while self.thread.is_alive():
                try:
                    self.extract.extract_content(1000)  # if video extract a frame each 1 seconds
                    self.thread.update_progressbar()
                    self.extract.crop_face()
                    self.thread.update_progressbar()
                    self.extract.align_face()
                    self.thread.update_progressbar()
                except Exception as e:
                    print('An error occurred during extraction procedure : {}'.format(e))
                break
        except Exception as e:
            print("An Error occurred during extraction : {}".format(e))
            self.thread.dialog.Destroy()
            self.dialog = Dialog("Fenêtre d'Erreur")
            self.dialog.show_dialog()

    def launch_analyze(self):
        """
        Launch the analyze of the extracted content
        :return: is_fake, confidence and artifact
        """
        try:
            while self.thread.is_alive():
                self.analyse.analyze_with_auto_encoder_model_detector(self.path_to_faces)
                self.thread.update_progressbar()
                self.analyse.analyze_with_gan_model_detector(self.path_to_images)
                self.thread.update_progressbar()
                self.analyse.compute_analyze()
                break
            self.thread = None
            return self.analyse.is_fake, self.analyse.confidence, self.analyse.artifact
        except Exception as e:
            print("An Error occurred during analyse : {}".format(e))
            self.thread.dialog.Destroy()
            self.dialog = Dialog("Fenêtre d'Erreur")
            self.dialog.show_dialog()
            raise


def main():
    """The main function"""
    app = wx.App()
    view = View(None, style=wx.DEFAULT_FRAME_STYLE)
    view.Show()
    app.MainLoop()


if __name__ == '__main__':
    main()
