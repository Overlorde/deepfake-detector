# -*- coding: utf-8 -*-
import logging
import os
import random
import keras
import numpy as np
from tensorflow.keras.applications import ResNet50
from tf_explain.core import GradCAM
import tensorflow as tf
from definitions import ROOT_DIR
from detector.extract.extract import Extract


class Analyze:
    """
        This class analyze the content
    """

    def __init__(self):
        """Constructor of Analyze"""
        application_path = os.path.join(ROOT_DIR, 'detector/train/model')
        self.is_fake = False
        self.confidence = None
        self.artifact = []
        self.path_to_model = [os.path.join(application_path, 'model_v1_ResNet50'),
                              os.path.join(application_path, 'model_v1_CNN_GAN')]
        self.text_labels = ['deepfake', 'real_face']
        self.result_detector_auto_encoder = []
        self.result_detector_gan = []
        self.explainer = GradCAM()

    def compute_analyze(self):
        """
        Give the final result of the analyze between the existing deep learning models prediction
        :return:
        """
        if np.around(self.result_detector_auto_encoder[0], decimals=3) == np.around(self.result_detector_gan[0],
                                                                                    decimals=3) and \
                self.result_detector_auto_encoder[1] != self.result_detector_gan[1]:
            print("Error, both confidences are equal. Models are not able to decide.")
            raise
        # prediction model comparison algorithm
        elif self.result_detector_auto_encoder[0] > self.result_detector_gan[0]:
            if self.result_detector_auto_encoder[1] == self.text_labels[0]:
                self.is_fake = True
            else:
                self.is_fake = False
            self.confidence = self.result_detector_auto_encoder[0]
            self.artifact = self.artifact[0]
        elif self.result_detector_auto_encoder[0] < self.result_detector_gan[0]:
            if self.result_detector_gan[1] == self.text_labels[0]:
                self.is_fake = True
            else:
                self.is_fake = False
            self.confidence = self.result_detector_gan[0]
            self.artifact = self.artifact[1]
        logging.info('computing the results done.')

    def analyze_with_gan_model_detector(self, path):
        """
        Analyze with the GAN model
        :param path: the path of the image to analyze
        :return:
        """
        try:
            new_model = tf.keras.models.load_model(self.path_to_model[1])
            new_model.build(input_shape=(None, 256, 256, 3))
            extract = Extract()
            predictions = []
            confidences = []
            for frames in os.listdir(path):
                extract.path_to_content = os.path.join(path, frames)
                co_occurrence_matrix = extract.extract_co_occurrence_matrix()
                x = np.expand_dims(co_occurrence_matrix[random.randrange(0, 4)].T, axis=0).astype('float32')
                predictions.append([(abs(0.5 - new_model.predict(x))), x])
            for prediction in predictions:
                confidences.append(prediction[0])  # max between 0 and 1
                # number of array is equal to preds and return x
            prediction = predictions[confidences.index(max(confidences))]  # if multiple max take the first one
            label_class = self.text_labels[int(new_model.predict_classes(prediction[1])[0])]
            if prediction[0].ravel() < 0.25:
                self.result_detector_gan.append(1 - prediction[0].ravel())
            else:
                self.result_detector_gan.append(0.5 + prediction[0].ravel())
            self.result_detector_gan.append(label_class)
            print(self.result_detector_gan[0])
            print(self.result_detector_gan[1])
            print(np.shape(prediction[1]))
            grid = self.explainer.explain((prediction[1], None), new_model, class_index=0,
                                          layer_name="conv2d_5")
            logging.info('prediction with CNN for detecting GAN done.')
            self.artifact.append(grid)
        except Exception as e:
            print('An error occurred during auto-encodeur detection : {}'.format(e))

    def analyze_with_auto_encoder_model_detector(self, path):
        """
        Analyze with the auto-encoder model
        :param path: the path of the image to analyze
        :return:
        """
        try:
            new_model = tf.keras.models.load_model(self.path_to_model[0])
            base_model = ResNet50(include_top=False, input_shape=(224, 224, 3), weights='imagenet')
            resnet50_layer = new_model.get_layer('resnet50')
            base_model.set_weights(resnet50_layer.get_weights())
            # base_model.summary()
            predictions = []
            confidences = []
            for frames in os.listdir(path):
                img = keras.preprocessing.image.load_img(os.path.join(path, frames), target_size=(224, 224))
                img = keras.preprocessing.image.img_to_array(img)
                img = img / 255
                img_array = np.expand_dims(img, axis=0)
                x = np.vstack([img_array])
                predictions.append([(abs(0.5 - new_model.predict(x))), x, img])
            for prediction in predictions:
                confidences.append(prediction[0])  # max between 0 and 1
                # number of array is equal to preds and return x
            prediction = predictions[confidences.index(max(confidences))]  # if multiple maximum take the first one
            label_class = self.text_labels[int(new_model.predict_classes(prediction[1])[0])]
            if prediction[0].ravel() < 0.25:
                self.result_detector_auto_encoder.append(1 - prediction[0].ravel())
            else:
                self.result_detector_auto_encoder.append(0.5 + prediction[0].ravel())
            self.result_detector_auto_encoder.append(label_class)
            print(self.result_detector_auto_encoder[0])
            print(self.result_detector_auto_encoder[1])
            print(np.shape([prediction[2]]))
            grid = self.explainer.explain((np.array([prediction[2]]), None), base_model, class_index=1,
                                          layer_name="conv5_block3_3_conv")
            logging.info('prediction with ResNet50 for detecting auto-encoder done.')
            self.artifact.append(grid)
        except Exception as e:
            print('An error occurred during GAN detection : {}'.format(e))
