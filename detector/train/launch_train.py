import os
import sys
from detector.train.model.model import Model
from detector.train.train import Train

if __name__ == '__main__':
    """
        The main function to execute the training process
        Options are :
        -i "path_to_dataset_train" (must be first in args)
        -v "path_to_dataset_valid" (must be second in args)
        -t "path_to_dataset_test" (must be third in args)
        -c 1 "CNN detector for auto-encoder"
           2 "CNN-LSTM auto-encoder"
           3 "CNN for Generative adversary network (GAN)"
        -o 0 "train, validate and test"
           1 "test only"
        -m "path to pre-trained model"
        -b "batch size"
        -e "number of epochs"
    """
    input_folder_dataset = []
    choice = None
    evaluate = None
    batch_size = None
    num_epochs = None
    path_to_model = None
    if len(sys.argv) >= 5:
        for count, argv in enumerate(sys.argv):
            if argv == '-i':
                input_folder_dataset.append(str(sys.argv.pop(count + 1)))
            elif argv == '-c':
                choice = int(sys.argv.pop(count + 1))
            elif argv == '-v':
                input_folder_dataset.append(str(sys.argv.pop(count + 1)))
            elif argv == '-t':
                input_folder_dataset.append(str(sys.argv.pop(count + 1)))
            elif argv == '-o':
                evaluate = int(sys.argv.pop(count + 1))
            elif argv == '-b':
                batch_size = int(sys.argv.pop(count + 1))
            elif argv == '-m':
                path_to_model = str(sys.argv.pop(count + 1))
            elif argv == '-e':
                num_epochs = int(sys.argv.pop(count + 1))
        try:
            for input_folder in input_folder_dataset:
                if not os.path.exists(str(input_folder)):
                    print("Dataset file not found.")
                    exit(-1)
            train = Train(model=Model(Model.available_models(choice), num_epochs, batch_size),
                          input_content=input_folder_dataset, mode=evaluate)
            if train is not None:  # the training procedure start here
                if evaluate == 0:
                    train_labels, train_predictions_baseline, valid_labels, validation_predictions_baseline = \
                        train.train_model()
                    test_labels, test_predictions_baseline = train.test_model()
                    train.evaluate_auc_model(train_predictions_baseline=train_predictions_baseline,
                                             validation_predictions_baseline=validation_predictions_baseline,
                                             test_predictions_baseline=test_predictions_baseline,
                                             train_labels=train_labels, valid_labels=valid_labels,
                                             test_labels=test_labels)
                elif evaluate == 1:
                    if path_to_model is not None:
                        test_labels, test_predictions_baseline = train.test_model(path_to_model=path_to_model)
                        train.evaluate_auc_model(train_predictions_baseline=None,
                                                 validation_predictions_baseline=None,
                                                 test_predictions_baseline=test_predictions_baseline,
                                                 train_labels=None, valid_labels=None,
                                                 test_labels=test_labels)
                    else:
                        print("Error, path to model missing.")
                else:
                    print("Value for -t argument not recognized : {}".format(evaluate))
            else:
                print("Error, it seems that something happened during the definition of the deep learning model.")
        except Exception as e:
            print("An error occurred : {}".format(e))
    else:
        print("Not enough arguments found.")
        exit(-1)
