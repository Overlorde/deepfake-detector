import os
import pathlib
import numpy as np
from detector.extract.extract import Extract
import sys

TRAIN_SIZE = 0.70
VALIDATION_SIZE = 0.15
TEST_SIZE = 0.15

if __name__ == '__main__':
    """
        This script allows to extract frames and faces from videos or co-occurrence matrices from frames
        Options are :
        -o "path_to_output_file"
        -i "path_to_input_file"
        -f "path_to_faces_file"
        -c 2 "Extract co-occurrence matrices from frames"
           1 "Extract frames and faces from videos"
        Notice : one input for option 1 and two input for option 2 (deepfake/real_face)
    """
    output_file = None
    input_folder_1 = None
    input_folder_2 = None
    face_file = None
    selection_operation = None
    if len(sys.argv) > 3:
        for count, argv in enumerate(sys.argv):
            if argv == '-c':
                selection_operation = int(sys.argv.pop(count + 1))
            elif argv == '-o':
                output_file = str(sys.argv.pop(count + 1))
            elif argv == '-f':
                face_file = str(sys.argv.pop(count + 1))
            elif argv == '-i':
                if input_folder_1 is None:
                    input_folder_1 = str(sys.argv.pop(count + 1))
                else:
                    input_folder_2 = str(sys.argv.pop(count + 1))
        if selection_operation == 1:
            print("Module for extracting faces from a specific folder selected.")
            if (input_folder_1 and input_folder_2) is not None or face_file is None:
                print("Two paths found where one is expected or face folder missing.")
                exit(1)
            try:
                if os.path.exists(input_folder_1):
                    if os.path.exists(output_file):
                        file = pathlib.Path(input_folder_1)
                        print("number of content found : " + str(len(list(file.glob('*')))))
                        extractor = None
                        for count, video in enumerate(os.listdir(input_folder_1)):
                            extractor = Extract(os.path.join(input_folder_1, video), output_file, face_file,
                                                None, None)
                            extractor.extract_content(100)  # extract a frame each 0.1 s
                            print("Video n°{} extracted. Progression : {} %".format(count, (
                                    count / len(os.listdir(input_folder_1)) * 100)))
                        if extractor is not None:
                            extractor.crop_face()
                        else:
                            print("Folder not found.")
            except Exception as e:
                print("An exception occurred in extraction module : {}".format(e))
        elif selection_operation == 2:
            print("Module for extracting co-occurrence matrix selected.")
            try:
                if os.path.exists(input_folder_1) and os.path.exists(input_folder_2) and (
                        TRAIN_SIZE + VALIDATION_SIZE + TEST_SIZE) == 1:
                    if not os.path.exists(output_file):
                        os.makedirs(output_file)
                    extractor = Extract(input_folder_1, output_file, None, None, None)
                    filename = os.path.join(output_file, 'dataset')

                    size_dataset_deep_fake = len(os.listdir(input_folder_1))
                    size_dataset_real_face = len(os.listdir(input_folder_2))

                    for count, image in enumerate(os.listdir(input_folder_1)):
                        extractor.path_to_content = os.path.join(input_folder_1, image)
                        file = filename + '_deepfake_' + str(count)
                        if count < TRAIN_SIZE * size_dataset_deep_fake:
                            np.savez_compressed(file=file, x_train=extractor.extract_co_occurrence_matrix(),
                                                y_train=False)
                        elif TRAIN_SIZE * size_dataset_deep_fake <= count < (
                                TRAIN_SIZE + VALIDATION_SIZE) * size_dataset_deep_fake:
                            np.savez_compressed(file=file, x_validation=extractor.extract_co_occurrence_matrix(),
                                                y_validation=False)
                        elif count >= (TRAIN_SIZE + VALIDATION_SIZE) * size_dataset_deep_fake:
                            np.savez_compressed(file=file, x_test=extractor.extract_co_occurrence_matrix(),
                                                y_test=False)
                        print('percentage done on first dataset : {} %'.format((count / size_dataset_deep_fake) * 100))
                    print('first dataset extraction done.')
                    for count, image in enumerate(os.listdir(input_folder_2)):
                        extractor.path_to_content = os.path.join(input_folder_2, image)
                        file = filename + '_real_face_' + str(count)
                        if count < TRAIN_SIZE * size_dataset_real_face:
                            np.savez_compressed(file=file, x_train=extractor.extract_co_occurrence_matrix(),
                                                y_train=True)
                        if TRAIN_SIZE * size_dataset_real_face <= count < (
                                TRAIN_SIZE + VALIDATION_SIZE) * size_dataset_real_face:
                            np.savez_compressed(file=file, x_validation=extractor.extract_co_occurrence_matrix(),
                                                y_validation=True)
                        if count >= (TRAIN_SIZE + VALIDATION_SIZE) * size_dataset_real_face:
                            np.savez_compressed(file=file, x_test=extractor.extract_co_occurrence_matrix(),
                                                y_test=True)
                        print(
                            'percentage done on second dataset : {} %'.format(
                                (count / size_dataset_real_face) * 100))
                    print('second dataset extraction done.')
                else:
                    print("Error with input_file or output file path.")
            except Exception as e:
                print("An exception occurred in extraction module : {}".format(e))
else:
    print("Error : Not enough args provided.")
