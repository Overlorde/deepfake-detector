import logging
import os
import random
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from sklearn.metrics import roc_curve, auc
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from detector.train.model.model import Model


class Train:
    """
        This class allows to train the preconfigured models, save them and load them
    """
    logging.basicConfig(filename='train.log', level=logging.INFO)

    def __init__(self, model, input_content, mode):
        """
        Constructor of Train
        :param model: the chosen model
        :param input_content: the input content (database)
        :param mode: the using mode (train and test or only test)
        """
        self.model = model
        self.input_content = input_content
        self.train_dataset = None
        self.validation_dataset = None
        self.test_dataset = None
        self.train_size, self.val_size, self.test_size = self.load_dataset(
            mode=mode)  # dispatch data to the different purpose

    def evaluate_auc_model(self, train_predictions_baseline, validation_predictions_baseline, test_predictions_baseline,
                           train_labels, valid_labels, test_labels):
        """
        Evaluate the model by using the AUC metric from TensorFlow
        :param test_labels: ground truth from the test database
        :param train_labels: ground truth from the train database
        :param valid_labels: ground truth from the validation database
        :param test_predictions_baseline: the predictions on the test dataset once the model has been trained
        :param validation_predictions_baseline: the predictions on the train dataset once the model has been trained
        :param train_predictions_baseline: the predictions on the validation dataset once the model has been trained
        :return:
        """
        plt.plot([0, 1], [0, 1], 'k--')
        if train_predictions_baseline is not None and train_labels is not None:
            self.plot_roc("Train Baseline", np.multiply(train_labels, 1), train_predictions_baseline, color='orange')
        if validation_predictions_baseline is not None and valid_labels is not None:
            self.plot_roc("Validation Baseline", np.multiply(valid_labels, 1), validation_predictions_baseline,
                          color='navy',
                          linestyle='--')
        if test_predictions_baseline is not None and test_labels is not None:
            self.plot_roc("Test Baseline", np.multiply(test_labels, 1), test_predictions_baseline, color='red',
                          linestyle='--')
        plt.show()

    @staticmethod
    def plot_roc(name, labels, predictions, **kwargs):
        """
        Calculate the AUC metric and plot the roc curve
        :param name: the name of the curve
        :param labels: the dataset labels
        :param predictions: the predicted labels of the model
        :param kwargs: the kwargs
        :return:
        """
        fp, tp, _ = roc_curve(labels, predictions)
        auc_rf = auc(fp, tp)
        plt.plot(fp, tp, label=name + ' ' + '(area = {:.3f})'.format(auc_rf), linewidth=2, **kwargs)
        plt.xlabel('False positives [%]')
        plt.ylabel('True positives [%]')
        plt.grid(True)
        plt.legend(loc='lower right')

    def test_model(self, path_to_model=None):
        """
        Evaluate the model on the test dataset
        :param path_to_model: the path to the pre-trained model
        :return: the ground truth of labels and the predicted labels of the model prediction
        """
        if path_to_model:
            self.model.model_architecture = tf.keras.models.load_model(path_to_model)
            self.model.model_architecture.compile(optimizer=self.model.optimizer,
                                                  loss='binary_crossentropy',
                                                  metrics=['accuracy'])
        else:
            results = self.model.model_architecture.evaluate(self.test_dataset,
                                                             steps=self.test_size // self.model.batch_size)
            print('test loss, test acc:', results)
        (test_images, test_labels) = next(iter(self.test_dataset))
        if self.model.name == Model.available_models(3):
            test_images = tf.dtypes.cast(test_images, tf.float32)
        # the entire dataset is predicted
        test_predictions_baseline = self.model.model_architecture.predict(test_images, verbose=1)
        if self.model.name == Model.available_models(1):
            test_predictions_baseline = test_predictions_baseline[:, 0]

        return test_labels, test_predictions_baseline

    def train_model(self):
        """
        Compile and launch the training and validation of the selected model
        :return: the ground truth labels and the predicted labels of the model
        """
        self.model.model_architecture.compile(optimizer=self.model.optimizer,
                                              loss='binary_crossentropy',
                                              metrics=['accuracy'])
        baseline_history = self.model.model_architecture.fit(self.train_dataset,
                                                             epochs=self.model.epoch,
                                                             steps_per_epoch=self.train_size // self.model.batch_size,
                                                             validation_steps=self.val_size // self.model.batch_size,
                                                             validation_data=self.validation_dataset,
                                                             callbacks=[
                                                                 tf.keras.callbacks.EarlyStopping(
                                                                     monitor='val_loss',
                                                                     verbose=1,
                                                                     patience=30,
                                                                     mode='min',
                                                                     restore_best_weights=True),
                                                                 tf.keras.callbacks.ModelCheckpoint(
                                                                     'model_v1_' + self.model.name,
                                                                     monitoWr='val_loss',
                                                                     verbose=True,
                                                                     save_best_only=True)])
        logging.info('\nhistory dict:', baseline_history.history)
        self.plot_metrics(baseline_history)
        (train_images, train_labels) = next(iter(self.train_dataset))
        (valid_images, valid_labels) = next(iter(self.validation_dataset))

        train_predictions_baseline = self.model.model_architecture.predict(train_images)
        validation_predictions_baseline = self.model.model_architecture.predict(valid_images)

        if self.model.name == Model.available_models(1):
            train_predictions_baseline = train_predictions_baseline[:, 0]
            validation_predictions_baseline = validation_predictions_baseline[:, 0]

        return train_labels, train_predictions_baseline, valid_labels, validation_predictions_baseline

    @staticmethod
    def get_data_from_filename(filename):
        """
        Get the data from a .npz file (matrix, label)
        :param filename: the .npz filename
        :return: the data and label of the .npz file
        """
        np_data = np.load(filename)
        return np_data[str(np_data.files[0])][random.randrange(0, 4)].T, np.reshape(np_data[str(np_data.files[1])], 1)

    def get_data_wrapper(self, filename):
        """
        Load and extract matrices, label from each .npz file and convert to tensor
        :param filename: the filename of the .npz file
        :return: the tensor associated to the content of the .npz file
        """
        features, labels = tf.numpy_function(
            self.get_data_from_filename, [filename],
            (tf.uint32, tf.bool))
        features.set_shape([256, 256, 3])
        labels.set_shape([1])
        return tf.data.Dataset.from_tensors((features, labels))

    @staticmethod
    def plot_metrics(history):
        """
        Plot the accuracy and loss of both dataset train and validation from each epoch
        :param history: the history of the model such as the loss and accuracy over the epochs
        :return:
        """
        # Plot training & validation accuracy values
        plt.plot(history.history['accuracy'])
        plt.plot(history.history['val_accuracy'])
        plt.title('Model accuracy')
        plt.ylabel('Accuracy')
        plt.xlabel('Epoch')
        plt.legend(['Train', 'Valid'], loc='upper left')
        plt.show()

        # Plot training & validation loss values
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title('Model loss')
        plt.ylabel('Loss')
        plt.xlabel('Epoch')
        plt.legend(['Train', 'Valid'], loc='upper left')
        plt.show()

    def load_dataset(self, mode):
        """
        The input dataset is transformed into tensors
        and converted to the correct content, label TensorFlow format.
        :param mode: the selected mode (train,validation,test or test)
        :return: train_size, validation_size, test_size (dataset sizes)
        """
        train_size = 0
        validation_size = 0
        test_size = 0
        is_shuffle = True
        if self.model.name == Model.available_models(1) or self.model.name == Model.available_models(2):
            if self.model.name == Model.available_models(1):
                target_size = (224, 224)
            elif self.model.name == Model.available_models(2):
                target_size = (1, 299, 299)
            else:
                print('An error occurred when choosing target size.')
                raise
            if mode == 0:
                train_image_generator = ImageDataGenerator(rescale=1. / 255)
                validation_image_generator = ImageDataGenerator(rescale=1. / 255)
                self.train_dataset = train_image_generator.flow_from_directory(batch_size=self.model.batch_size,
                                                                               directory=self.input_content[0],
                                                                               shuffle=is_shuffle,
                                                                               target_size=target_size,
                                                                               class_mode='binary')
                self.validation_dataset = validation_image_generator.flow_from_directory(
                    batch_size=self.model.batch_size,
                    directory=self.input_content[
                        1],
                    shuffle=is_shuffle,
                    target_size=target_size,
                    class_mode='binary')
                train_size = len(list(self.train_dataset.filenames))
                validation_size = len(list(self.validation_dataset.filenames))
            test_image_generator = ImageDataGenerator(rescale=1. / 255)
            self.test_dataset = test_image_generator.flow_from_directory(batch_size=self.model.batch_size,
                                                                         shuffle=is_shuffle,
                                                                         directory=self.input_content[-1],
                                                                         target_size=target_size,
                                                                         class_mode='binary')
            test_size = len(list(self.test_dataset.filenames))

        elif self.model.name == Model.available_models(3):
            if mode == 0:
                validation_files = []
                train_files = []

                files1 = os.listdir(str(self.input_content[0]))
                files2 = os.listdir(str(self.input_content[1]))

                for file in files1:
                    train_files.append(os.path.join(str(self.input_content[0]), file))
                for file in files2:
                    validation_files.append(os.path.join(str(self.input_content[1]), file))
                random.shuffle(train_files)
                random.shuffle(validation_files)
                train_dataset = tf.data.Dataset.from_tensor_slices(train_files).flat_map(self.get_data_wrapper)
                validation_dataset = tf.data.Dataset.from_tensor_slices(validation_files).flat_map(
                    self.get_data_wrapper)
                train_size = len(list(files1))
                validation_size = len(list(files2))
                # repeat allows to integrate all the dataset, when done signify the end of an epoch
                self.train_dataset = train_dataset.repeat().shuffle(1000).batch(self.model.batch_size)
                self.validation_dataset = validation_dataset.repeat().shuffle(1000).batch(self.model.batch_size)
                print("train dataset size : {}".format(train_size))
                print("validation dataset size : {}".format(validation_size))
            test_files = []
            files3 = os.listdir(str(self.input_content[-1]))
            for file in files3:
                test_files.append(os.path.join(str(self.input_content[-1]), file))
            test_size = len(list(files3))
            random.shuffle(test_files)
            test_dataset = tf.data.Dataset.from_tensor_slices(test_files).flat_map(self.get_data_wrapper)
            self.test_dataset = test_dataset.repeat().shuffle(1000).batch(self.model.batch_size)
            print("test dataset size : {}".format(test_size))
        if (train_size or validation_size or test_size) >= 30000:
            logging.warning('WARNING : AUC might fail with a memory dump due to the size above 30000 samples.')
        return train_size, validation_size, test_size
