from tensorflow.keras.applications import ResNet50, InceptionV3
from tensorflow.keras.layers import LSTM, Flatten, Conv2D, MaxPooling2D, Dense, Activation, Dropout, TimeDistributed
from tensorflow.keras.models import Sequential
import tensorflow as tf


class Model:
    """
        This class allows to define the AI model
    """

    @staticmethod
    def available_models(choice: int):
        """
        Display the different deep learning model implemented
        :param choice: the chosen model name
        :return: the model name
        """
        models = ['ResNet50', 'LSTM', 'CNN_GAN']
        return models[choice - 1]

    def __init__(self, name: str, epoch: int, batch_size: int):
        """
        Constructor of Model
        :param name: the name of the chosen model
        :param epoch: the number of epochs
        :param batch_size: the size of the batch size
        """
        self.name = name
        self.model_architecture = None
        self.optimizer = None
        if epoch > 0:
            self.epoch = epoch
        else:
            print('epoch doît être supérieur à zéro.')
            raise
        if epoch > 0:
            self.batch_size = batch_size
        else:
            print('batch_size doît être supérieur à zéro.')
            raise
        if self.name == Model.available_models(1):
            try:
                base_model = ResNet50(include_top=False, input_shape=(224, 224, 3), weights='imagenet')
                flatten_layer = Flatten()  # block of features found on the image
                prediction_layer = tf.keras.layers.Dense(1, activation='sigmoid')
                self.optimizer = tf.keras.optimizers.SGD(tf.keras.optimizers.schedules.ExponentialDecay(
                    initial_learning_rate=0.001,
                    decay_steps=1000,
                    decay_rate=0.95,
                    staircase=False))

                self.model_architecture = Sequential([
                    base_model,
                    flatten_layer,
                    prediction_layer
                ])
                for layer in base_model.layers:
                    layer.trainable = True
                for layer in base_model.layers:
                    print(layer.name, layer.trainable)
                print("Number of layers in the base model: ", len(base_model.layers))
            except Exception as e:
                print("An Error occurred during AI pipeline creation : {}".format(e))
        elif self.name == Model.available_models(2):  # be careful, model is not working with validation
            try:
                image = tf.keras.layers.Input(shape=(20, 299, 299, 3), name='image_input')
                cnn = InceptionV3(
                    weights='imagenet',
                    include_top=False,
                    pooling='max')
                cnn.trainable = False
                encoded_frame = TimeDistributed(tf.keras.layers.Lambda(lambda x: cnn(x)))(image)
                encoded_vid = LSTM(units=2048, dropout=0.5)(encoded_frame)
                layer1 = Dense(units=512)(encoded_vid)
                dropout1 = Dropout(0.5)(layer1)
                layer2 = Dense(512, activation='relu')(dropout1)
                dropout2 = Dropout(0.5)(layer2)
                outputs = Dense(1, activation='sigmoid')(dropout2)
                self.model_architecture = tf.keras.models.Model(inputs=[image], outputs=outputs)
                self.optimizer = tf.keras.optimizers.Adam(learning_rate=0.00001, decay=0.000001)
            except Exception as e:
                print("An Error occurred during AI pipeline creation : {}".format(e))
        elif self.name == Model.available_models(3):
            try:
                layer1 = Conv2D(32, (3, 3), input_shape=(256, 256, 3), data_format='channels_last')
                activation1 = Activation('relu')
                layer2 = Conv2D(32, (5, 5))
                layer3 = MaxPooling2D()
                layer4 = Conv2D(64, (3, 3))
                activation2 = Activation('relu')
                layer5 = Conv2D(64, (5, 5))
                layer6 = MaxPooling2D()
                layer7 = Conv2D(128, (3, 3))
                activation3 = Activation('relu')
                layer8 = Conv2D(128, (5, 5))
                layer9 = MaxPooling2D()
                layer10 = Dense(256)
                layer11 = Dense(256)
                layer_flatten = Flatten()
                layer12 = Dense(1)
                layer13 = Activation('sigmoid')
                self.model_architecture = Sequential([layer1,
                                                      activation1,
                                                      layer2,
                                                      layer3,
                                                      layer4,
                                                      activation2,
                                                      layer5,
                                                      layer6,
                                                      layer7,
                                                      activation3,
                                                      layer8,
                                                      layer9,
                                                      layer10,
                                                      layer11,
                                                      layer_flatten,
                                                      layer12,
                                                      layer13])
                self.optimizer = tf.keras.optimizers.Adadelta(learning_rate=1.0, rho=0.95)
            except Exception as e:
                print("An error occurred during AI pipeline creation : {}".format(e))
        else:
            print('Error, unknown model.')
            raise
        if self.model_architecture is not None:
            self.model_architecture.summary()
