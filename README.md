# DeepFake Detector

This project is aimed at detecting DeepFake content (based on autoencoder and GAN) thanks to research papers methods

## Interface

![Alt text](interface.png?raw=true "Interface")


## Getting Started

Clone the repo, compile the source code and enjoy !

### Prerequisites

Python version must at least 3.x \
**Note : TensorFlow is not implemented in Python 3.8** \
Package required for training are :
- tensorflow 2.1.0
- tensorboard 2.1.0
- tensorflow-gpu 2.1.0 (optional)
- keras 2.3.1
- scikit-learn 0.21.2
- cv2 4.2.1.30
- numpy 1.16.4
- matplotlib 3.1.0

Package required for using the interface :
- tensorflow 2.1.0
- tensorboard 2.1.0
- tensorflow-gpu 2.1.0 (optional)
- keras 2.3.1
- scikit-learn 0.21.2
- cv2 4.2.1.30
- numpy 1.16.4
- matplotlib 3.1.0
- tf-explain 2.1.0
- dlib (installation with Conda is recommended)
- wxPython 4.0.7

In order to use tensorflow-gpu:
- Have an NVIDIA graphic card
- CuDNN version 10.0.0 (https://developer.nvidia.com/rdp/cudnn-archive)
- CUDA 10.0.0 (https://developer.nvidia.com/cuda-10.0-download-archive)
- Other versions above 10.X.X might work but have not been tested

Tutorial for installing CuDNN and CUDA are :
- Linux : https://docs.nvidia.com/deeplearning/sdk/cudnn-install/index.html
- Windows : https://docs.nvidia.com/deeplearning/sdk/cudnn-install/index.html#install-windows
- Don't forget to apply for membership at : https://developer.nvidia.com/developer-program

### Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the packages listed above.

```bash
pip install package-name
```
## Pre-process the dataset
This script allows to extract frames from videos or co-occurrence matrices. \
There are 2 options available :
1.	Extract images and faces from videos or images
2.	Extract co-occurrence matrices from images

When the extraction is over, the division between each dataset base will have to be done manually.\
Available arguments :
```bash
-i : Placeholder of the files to extract
-o : Placeholder of the output files
-t : Placeholder of the faces folder output
-c : Model (1 = option 1, 2 = option 2)
```
Command sample with option 1 :
```bash
-i C:\Dataset\images -f C:\Dataset\faces -o C:\Dataset\temp
```
Command sample with option 2 :
```bash
-i C:\Dataset\deepfake -i C:\Dataset\real_face -o C:\Dataset\dataset
```
Be careful, order -i deepfake -i real_face is mandatory.
## Launch the training/test
This script allows to train/test the chosen deep learning model. \
There are 2 options available :
1.	Train, validate and test the selected deep learning model
2.	Test the selected deep learning model \

Available arguments :
```bash
-i : Placeholder of the training files
-v : Placeholder of the validation files
-t : Placeholder of the test files
-c : Model selection (1 and 3 actually implemented)
-o : Mode (0 = train, validate, test, 1 = test only)
-b : batch size for each dataset bases
-e : number of epochs
-m : Placeholder of the deep learning pre-trained model
```
Command sample with option 1 :
```bash
-i C:\Dataset\train -v C:\Dataset\valid -t C:\Dataset\test -c 3 -o 0 -b 40 -e 50
```
Be careful, order -i, -v, -t is mandatory.\
Command sample with option 2 :
```bash
-m C:\model\modele_ResNet50 -t C:\Dataset\test -c 3 -o 1 -b 40 -e 50
```
## Running the tests

run the command below in folder deepfake_detector/tests
```bash
pytest filename.py
```

## Versioning

Version 1.0

## Authors

* **Burnier-Framboret Alexandre** - *Initial work* - [Overlorde](https://gitlab.com/Overlorde)

## License

This project is not under license.

## Acknowledgments

* Created with Python love

