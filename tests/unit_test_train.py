import pytest


def test_should_load_dataset():
    pass  # not suitable for unit test


def test_should_train_model():
    pass # not suitable for unit test


def test_should_evaluate_model():
    pass # not suitable for unit test


def test_should_get_data_from_filename(train_setup):
    train = train_setup
    data, label = train.get_data_from_filename('./test_co_occurrence_matrix/dataset_deepfake_15001.npz')
    assert data is not None, 'get data from .npz file failed.'
    assert label is not None, 'get data from .npz file failed.'
