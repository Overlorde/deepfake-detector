import pytest
import os

@pytest.mark.skipif(os.getenv('DISPLAY', False) is False,
                    reason="does not run without a DISPLAY") # set to true if only not on gitlab
def test_should_launch_extract_then_launch_analyze(controller_setup):
    controller, os, cv2, np = controller_setup
    controller.launch_extract()
    controller.launch_analyze()
    assert len(os.listdir(controller.extract.path_to_images)) > 0, "launch_extract and launch_analyze failed."
    assert len(os.listdir(controller.extract.path_to_align_faces)) > 0, "launch_extract and launch_analyze failed."
    assert len(os.listdir(controller.extract.path_to_faces)) > 0, "launch_extract and launch_analyze failed."
    assert controller.analyse.confidence > 0, "launch_analyze failed."
    assert controller.analyse.artifact is not None, "launch_analyze failed."
    assert os.path.exists(controller.path_to_images) is True, "launch_extract and launch_analyze failed."
    assert os.path.exists(controller.path_to_align_faces) is True, "launch_extract and launch_analyze failed."
    assert os.path.exists(controller.path_to_faces) is True, "launch_extract and launch_analyze failed."
