import pytest

from detector.analyze.analyze import Analyze
from detector.interface.interface import Controller
from detector.train.model.model import Model
from detector.train.train import Train


@pytest.fixture
def model_setup(scope="module"):
    model = Model(batch_size=64, epoch=20, name='ResNet50')
    yield model


@pytest.fixture
def train_setup(scope="module"):
    import tensorflow as tf
    train = Train(tf.keras.models.load_model("../detector/train/model/model_v1_ResNet50"), None, 1)
    yield train
    pass


@pytest.fixture
def analyze_setup(scope="module"):
    analyse = Analyze()
    analyse.path_to_model = ["../detector/train/model/model_v1_ResNet50",
                             "../detector/train/model/model_v1_CNN_GAN"]
    yield analyse


@pytest.fixture
def controller_setup(scope="module"):
    import os
    import cv2
    import shutil
    import numpy as np
    from detector.interface.interface import View
    from detector.interface.interface import ProgressDialogThread
    import wx.lib.newevent
    app = wx.App()
    view = View(None)

    controller = Controller(path='./test_images/test_visage.jpg', path_to_images='./test_result_images',
                            path_to_faces='./test_result_faces', path_to_align_faces='./test_result_align_faces',
                            path_to_shape_predictor='../detector/extract/shape_predictor_68_face_landmarks.dat')
    controller.thread = ProgressDialogThread()
    if not os.path.exists(controller.path_to_images):
        os.mkdir(controller.path_to_images)
    if not os.path.exists(controller.path_to_align_faces):
        os.mkdir(controller.path_to_align_faces)
    if not os.path.exists(controller.path_to_faces):
        os.mkdir(controller.path_to_faces)
    controller.analyse.path_to_model = ["../detector/train/model/model_v1_ResNet50",
                                        "../detector/train/model/model_v1_CNN_GAN"]
    yield controller, os, cv2, np
    if os.path.exists(controller.path_to_faces):
        shutil.rmtree(controller.path_to_faces)
    if os.path.exists(controller.path_to_align_faces):
        shutil.rmtree(controller.path_to_align_faces)
    if os.path.exists(controller.path_to_images):
        shutil.rmtree(controller.path_to_images)
    wx.CallAfter(view.Close)
    app.MainLoop()


@pytest.fixture
def view_setup(scope="module"):
    import os
    import numpy as np
    import shutil
    from detector.interface.interface import View
    import wx.lib.newevent
    app = wx.App()
    view = View(None)
    view.controller = Controller(path='./test_images/test_visage.jpg', path_to_images='./test_result_images',
                                 path_to_faces='./test_result_faces', path_to_align_faces='./test_result_align_faces',
                                 path_to_shape_predictor='../detector/extract/shape_predictor_68_face_landmarks.dat')
    view.controller.analyse.path_to_model = ["../detector/train/model/model_v1_ResNet50",
                                             "../detector/train/model/model_v1_CNN_GAN"]
    yield app, view, wx, os, np
    wx.CallAfter(view.Close)
    app.MainLoop()
    if os.path.exists(view.controller.extract.path_to_images):
        shutil.rmtree(view.controller.extract.path_to_images)
    if os.path.exists(view.controller.extract.path_to_align_faces):
        shutil.rmtree(view.controller.extract.path_to_align_faces)
    if os.path.exists(view.controller.extract.path_to_faces):
        shutil.rmtree(view.controller.extract.path_to_faces)


@pytest.fixture
def extract_setup(scope="module"):
    import shutil
    import os
    import cv2
    import numpy as np
    from detector.extract.extract import Extract
    extract = Extract(path_to_content='./test_images/test_visage.jpg',
                      path_to_faces='./test_result_faces',
                      path_to_images='./test_result_images',
                      path_to_align_faces='./test_result_align_faces',
                      shape_predictor='../detector/extract/shape_predictor_68_face_landmarks.dat')

    test_frames = os.listdir('./test_images')
    img = []
    for test_frame in test_frames:
        img.append(cv2.imread('./test_images/' + test_frame, cv2.IMREAD_COLOR))
    yield os, cv2, img, extract, np, shutil
    if os.path.exists(extract.path_to_images):
        shutil.rmtree(extract.path_to_images)
    if os.path.exists(extract.path_to_align_faces):
        shutil.rmtree(extract.path_to_align_faces)
    if os.path.exists(extract.path_to_faces):
        shutil.rmtree(extract.path_to_faces)
