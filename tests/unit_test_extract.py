def test_constructor_extract(extract_setup):
    os, cv2, img, extract, np, shutil = extract_setup
    assert extract.path_to_faces == './test_result_faces', "path_to_faces is incorrect"
    assert extract.path_to_images == './test_result_images', "path_to_images is incorrect"
    assert extract.path_to_align_faces == './test_result_align_faces', "path_to_align_faces is incorrect"
    assert extract.path_to_content == './test_images/test_visage.jpg', "path_to_content is incorrect"


def test_should_extract_content(extract_setup):
    os, cv2, img, extract, np, shutil = extract_setup
    # Create the event
    extract.extract_content(1000)
    assert len(os.listdir(extract.path_to_images)) == 1, "extract_content failed to create repository"
    assert os.path.exists(extract.path_to_images) is True, "extract_content failed to create repository"
    extract.path_to_content = './test_video/01_02__exit_phone_room__YVGY8LOK.mp4'
    extract.extract_content(1000)
    assert len(os.listdir(extract.path_to_images)) == 10, "extract_content failed to create repository"


def test_should_crop_face(extract_setup):
    os, cv2, img, extract, np, shutil = extract_setup
    # Create the event
    os.mkdir('./test_images_temp/')
    for image in os.listdir('./test_images'):
        shutil.copy(os.path.join('./test_images', image), 'test_images_temp')

    extract.path_to_images = './test_images_temp'
    extract.crop_face()
    assert len(os.listdir(extract.path_to_faces)) == 1, "crop_face failed to crop face"


def test_should_align_face(extract_setup):
    os, cv2, img, extract, np, shutil = extract_setup
    # Create the event
    os.mkdir('./test_images_temp/')
    for image in os.listdir('./test_images'):
        shutil.copy(os.path.join('./test_images', image), 'test_images_temp')

    extract.path_to_faces = './test_images_temp'
    extract.align_face()
    assert os.path.exists(extract.path_to_align_faces) is True, "align_face failed to create repository"
    assert len(os.listdir(extract.path_to_align_faces)) == 1, "align_face failed to align face"


def test_should_blur_face(extract_setup):
    os, cv2, img, extract, np, shutil = extract_setup
    assert np.array_equal(extract.blur_face(), cv2.imread(
        extract.path_to_content, cv2.IMREAD_UNCHANGED)) is False, "blur_face failed to blur face"


def test_should_calculate_co_occurrence_matrix(extract_setup):
    os, cv2, img, extract, np, shutil = extract_setup
    # Create the event
    co_occurrence_matrix = extract.extract_co_occurrence_matrix()
    assert np.shape(co_occurrence_matrix) == (4, 3, 256, 256), "extract_co_occurrence_matrix failed."
