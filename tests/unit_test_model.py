def test_should_init_model(model_setup):
    model = model_setup
    assert model.batch_size == 64, "model constructor failed."
    assert model.epoch == 20, "model constructor failed."
    assert model.name == 'ResNet50', "model constructor failed."
    assert model.optimizer is not None, "model constructor failed."
    assert model.model_architecture is not None, "model constructor failed."
