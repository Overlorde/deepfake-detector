import pytest
import os


@pytest.mark.skipif(os.getenv('DISPLAY', False) is False,
                    reason="does not run without a DISPLAY")  # set to true if only not on gitlab
def test_should_launch_interface_and_browse_button_then_launch_detector(view_setup):
    pass
    app, view, wx, os, np = view_setup
    view.button_browse.SetValue('./test_images/test_visage.jpg')
    click_event = wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK)
    view.on_button(click_event)
    assert view.confidence_text.GetLabel() is not '', 'launch detector failed.'
    assert view.result_text.GetLabel() is not '', 'launch detector failed.'
    assert np.shape(view.image_artifact) == np.shape(np.zeros((224, 224, 3))), 'launch detector failed.'