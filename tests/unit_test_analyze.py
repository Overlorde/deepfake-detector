def test_should_compute_analyze(analyze_setup):
    analyze = analyze_setup
    analyze.result_detector_gan = [0.78, 'deepfake']
    analyze.result_detector_auto_encoder = [0.21, 'real_face']
    analyze.artifact = ['artifact1', 'artifact2']
    analyze.compute_analyze()
    assert analyze.is_fake is True, "compute_analyze failed."
    assert analyze.confidence == 0.78, "compute_analyze failed."
    assert analyze.artifact == 'artifact2', "compute_analyze failed."


def test_analyze_with_gan_model_detector(analyze_setup):
    analyze = analyze_setup
    analyze.analyze_with_gan_model_detector('./test_images')
    assert len(analyze.result_detector_gan) == 2, "analyze_with_gan_model_detector failed."


def test_analyze_with_auto_encoder_model_detector(analyze_setup):
    analyze = analyze_setup
    analyze.analyze_with_auto_encoder_model_detector('./test_images')
    assert len(analyze.result_detector_auto_encoder) == 2, "analyze_with_autoencoder_model_detector failed."
