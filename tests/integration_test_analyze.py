def test_should_launch_auto_encoder_detector_and_gan_detector_then_compute_analyze(analyze_setup):
    analyze = analyze_setup
    analyze.analyze_with_auto_encoder_model_detector('./test_images')
    analyze.analyze_with_gan_model_detector('./test_images')
    analyze.compute_analyze()
    assert len(analyze.result_detector_auto_encoder) == 2, "compute_analyze failed."
    assert len(analyze.result_detector_gan) == 2, "compute_analyze failed."
    assert analyze.confidence > 0, "compute_analyze failed."
    assert analyze.artifact is not None, "compute_analyze failed."
