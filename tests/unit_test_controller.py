import pytest
import os


@pytest.mark.skipif(os.getenv('DISPLAY', False) is False,
                    reason="does not run without a DISPLAY") # set to true if only not on gitlab
def test_should_launch_extract(controller_setup):
    controller, os, cv2, np = controller_setup
    controller.launch_extract()
    assert len(os.listdir(controller.path_to_images)) > 0, "launch_extract path_to_images failed."
    assert len(os.listdir(controller.path_to_align_faces)) > 0, "launch_extract path_to_align_faces failed."
    assert len(os.listdir(controller.path_to_faces)) > 0, "launch_extract path_to_faces failed."
    assert np.array_equal(cv2.imread(controller.path, cv2.IMREAD_UNCHANGED), cv2.imread(
        controller.path_to_images, cv2.IMREAD_UNCHANGED)) is False, "launch_extract extract_content failed."
    assert np.array_equal(cv2.imread(controller.path, cv2.IMREAD_UNCHANGED), cv2.imread(
        controller.path_to_faces, cv2.IMREAD_UNCHANGED)) is False, "launch_extract extract_faces failed."
    assert np.array_equal(cv2.imread(controller.path, cv2.IMREAD_UNCHANGED), cv2.imread(
        controller.path_to_align_faces, cv2.IMREAD_UNCHANGED)) is False, "launch_extract align_faces failed."
    controller.thread.dialog.Destroy()

@pytest.mark.skipif(os.getenv('DISPLAY', False) is False,
                    reason="does not run without a DISPLAY") # set to true if only not on gitlab
def test_should_launch_analyze(controller_setup):
    import shutil
    controller, os, cv2, np = controller_setup
    shutil.copyfile(controller.path,os.path.join(controller.path_to_faces,'face.jpg'))
    controller.launch_analyze()
    assert controller.analyse.artifact is not None, "launch_analyze failed."
    assert len(controller.analyse.result_detector_auto_encoder) == 2, "launch_analyze failed."
    assert len(controller.analyse.result_detector_gan) == 2, "launch_analyze failed."
    assert controller.analyse.confidence > 0, "launch_analyze failed."
    controller.thread.dialog.Destroy()
