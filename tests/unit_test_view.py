import pytest
import os


@pytest.mark.skipif(os.getenv('DISPLAY', False) is False,
                    reason="does not run without a DISPLAY")  # set to true if only not on gitlab
def test_on_filename_changed_disable(view_setup):
    app, view, wx, os, np = view_setup
    # Create the event
    click_event = wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK)
    click_event.String = 'this path is not correct'
    view.on_filename_changed(click_event)
    assert view.button_launch.IsEnabled() is False, "button_launch test failed."


@pytest.mark.skipif(os.getenv('DISPLAY', False) is False,
                    reason="does not run without a DISPLAY") # set to true if only not on gitlab
def test_on_filename_changed_enable(view_setup):
    app, view, wx, os, np = view_setup
    # Create the event
    click_event = wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK)
    click_event.String = os.getcwd()
    view.on_filename_changed(click_event)
    assert view.button_launch.IsEnabled() is True, "button_launch test failed."


@pytest.mark.skipif(os.getenv('DISPLAY', False) is False,
                    reason="does not run without a DISPLAY") # set to true if only not on gitlab
def test_on_button_contain_right_extension(view_setup):
    app, view, wx, os, np = view_setup
    view.button_browse.SetValue('exit.png')
    click_event = wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK)
    view.on_button(click_event)
    assert view.button_browse.GetValue() == 'exit.png', "button_browse test failed."


@pytest.mark.skipif(os.getenv('DISPLAY', False) is False,
                    reason="does not run without a DISPLAY") # set to true if only not on gitlab
def test_on_quit(view_setup):
    app, view, wx, os, np = view_setup
    click_event = wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK)
    assert view.quit(click_event) is True, "quit test failed."
