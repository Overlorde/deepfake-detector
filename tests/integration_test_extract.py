def test_should_extract_faces_and_crop_faces_and_align_from_video(extract_setup):
    # user must know that only one face must be present on video or image
    os, cv2, img, extract, np, shutil = extract_setup
    extract.extract_content(1000)
    assert extract.path_to_images is not None and len(os.listdir(extract.path_to_images)) == 1, 'extract_content failed.'

def test_should_extract_faces_and_crop_faces_and_align_from_image(extract_setup):
    os, cv2, img, extract, np, shutil = extract_setup
    extract.path_to_content = './test_video/01_02__exit_phone_room__YVGY8LOK.mp4'
    extract.extract_content(1000)
    assert extract.path_to_images is not None and len(os.listdir(extract.path_to_images)) > 1, 'extract_content failed.'
